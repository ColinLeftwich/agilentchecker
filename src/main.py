from os import listdir, getcwd
from os.path import isfile, join
from pathlib import Path
import csv

TEMP_FILE = "~"

# from openpyxl import load_workbook
class FilePath:
    full_path: str
    filename: str

    def __init__(self, parent_path: str, filename: str):
        self.full_path = join(parent_path, filename)
        self.filename = filename
    
class Group:
    id: int
    row: list[str]
    files: list[str]

    def __init__(self, num: int, row: list[str], files: list[str]):
        self.id = num
        self.row = row
        self.files = files


def main():
    path = Path(r"C:\Users\Colin\Documents\Work\Data\Keysight DAQ\B and C\4.5GPH and LOWER")

    data_files: list[FilePath] = []
    file_groups: list[Group] = []

    for filename in listdir(path):
        file_path = FilePath(path, filename)
        if isfile(file_path.full_path) and filename.endswith('.csv'):
            data_files.append(file_path)

    for data_file in data_files:
        if data_file.full_path[0] == TEMP_FILE:
            continue


        with open(data_file.full_path, 'r', encoding='utf-8') as file:
            reader = csv.reader(file)
            for row in reader:
                for cell in row:
                    if cell == "Scan Sweep Time (Sec)":
                        for group in file_groups:
                            if row == group.row:
                                group.files.append(data_file.filename)
                                break
                        else:
                            new_group = Group(len(file_groups), row, [data_file.filename])
                            file_groups.append(new_group)

    for group in file_groups:
        print(group.id)
        for files in group.files:
            print(f"Files: {files}")

if __name__ == "__main__":
    main()